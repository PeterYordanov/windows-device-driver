#include "ntddk.h"

#ifndef _CRT_SECURE_NO_WARNINGS
  #define _CRT_SECURE_NO_WARNINGS
#endif
#define DELAY_ONE_MILLISECOND (DELAY_ONE_MICROSECOND * 1000)
#define THREAD_COUNT 0x1 << 7

UINT64 structureSize = 0;
PSYSTEM_LOGICAL_PROCESSOR_INFORMATION_EX info = NULL;
ULONG cpuCount = 0;
HANDLE threads[THREAD_COUNT] = { 0 };
PVOID threadObjects[THREAD_COUNT] = { 0 };
BOOLEAN signal = FALSE;

UNICODE_STRING deviceName = RTL_CONSTANT_STRING(L"\\Device\\WindowsDeviceDriver01");
UNICODE_STRING symbolicLinkName = RTL_CONSTANT_STRING(L"\\??\\WindowsDeviceDriverLink01");
PDEVICE_OBJECT deviceObject;

NTSTATUS DriverEntry(PDRIVER_OBJECT driverObject, PUNICODE_STRING registryPath);
NTSTATUS DispatchPassThru(PDEVICE_OBJECT deviceObject, PIRP ioRequestPacket);
NTSTATUS DispatchDevControl(PDEVICE_OBJECT deviceObject, PIRP ioRequestPacket);
VOID     CreateSystemThread();
VOID     DriverUnload(PDRIVER_OBJECT driverObject);

NTSTATUS DriverEntry(PDRIVER_OBJECT driverObject, PUNICODE_STRING registryPath)
{
	ULONG size = 0;
	int cpuInfo[4] = { 0 };
	NTSTATUS status;
	driverObject->DriverUnload = DriverUnload;

	__cpuid(cpuInfo, 0);
	if (cpuInfo[1] != 0x765e6547 ||
		cpuInfo[2] != 0x6c65746e ||
		cpuInfo[3] != 0x49656e69)
	{
		DbgPrint("Not an Intel CPU \n");
		return STATUS_UNSUCCESSFUL;
	}

	__cpuid(cpuInfo, 1);
	if ((cpuInfo[3] & 0x20) == 0) {
		DbgPrint("Not supported Model Specific Register \n");
		return STATUS_UNSUCCESSFUL;
	}

	__cpuid(cpuInfo, 6);
	if ((cpuInfo[3] & 0x1) == 0) {
		DbgPrint("Not supported Digital Thermal Sensor \n");
		return STATUS_UNSUCCESSFUL;
	}

	status = KeQueryLogicalProcessorRelationship(NULL, RelationProcessorCore, NULL, &size);

	if(status == STATUS_INFO_LENGTH_MISMATCH) {
		info = (PSYSTEM_LOGICAL_PROCESSOR_INFORMATION_EX)ExAllocatePoolWithTag(PagedPool, size, 'abcd');
		if (!info)
			return STATUS_UNSUCCESSFUL;

		RtlZeroMemory(info, size);
		status = KeQueryLogicalProcessorRelationship(NULL, RelationProcessorCore, info, &size);

		if(!NT_SUCCESS(status)) {
			ExFreePool(info);
			return STATUS_UNSUCCESSFUL;
		}

		//TODO:
		//structureSize = (UINT64)();
		cpuCount = size / structureSize;
	}
	else {
		return STATUS_UNSUCCESSFUL;
	}

	CreateSystemThread();

	IoCreateDevice(driverObject, 0, &deviceName, FILE_DEVICE_UNKNOWN, 
                                                 FILE_DEVICE_SECURE_OPEN,
                                                 FALSE,
                                                 &deviceObject);

	if(!NT_SUCCESS(status)) {
		KdPrint(("Device Creation Failed \r\n"));
		return status;
	}

	status = IoCreateSymbolicLink(&symbolicLinkName, &deviceName);

	if (!NT_SUCCESS(status)) {
		KdPrint(("Symbolic Link Creation Failed \r\n"));
		IoDeleteDevice(&deviceObject);
		return status;
	}

	for (int i = 0; i < IRP_MJ_MAXIMUM_FUNCTION; i++) {
		driverObject->MajorFunction[i] = DispatchPassThru;
	}

	//driverObject->MajorFunction[IRP_MJ_READ] = DispatchPassThru;
	//driverObject->MajorFunction[IRP_MJ_WRITE] = DispatchPassThru;
	driverObject->MajorFunction[IRP_MJ_DEVICE_CONTROL] = DispatchDevControl;

	KdPrint(("Driver Loaded Successfully... \r\n"));

	return status;
}

NTSTATUS DispatchDevControl(PDEVICE_OBJECT deviceObject, PIRP ioRequestPacket)
{
#define DEVICE_SEND CTL_CODE(FILE_DEVICE_UNKNOWN, \
                             0x801, \
                             METHOD_BUFFERED, \
                             FILE_WRITE_DATA)

#define DEVICE_RECEIVE CTL_CODE(FILE_DEVICE_UNKNOWN, \
                                0x801 << 1, \
                                METHOD_BUFFERED, \
                                FILE_READ_DATA)

	PIO_STACK_LOCATION irpStackPointer = IoGetCurrentIrpStackLocation(ioRequestPacket);
	NTSTATUS status = STATUS_SUCCESS;

	WCHAR* sample = L"Returned from driver";
	PVOID buffer = ioRequestPacket->AssociatedIrp.SystemBuffer;
	ULONG inputBufferLength = irpStackPointer->Parameters.DeviceIoControl.InputBufferLength;
	ULONG outputBufferLength = irpStackPointer->Parameters.DeviceIoControl.OutputBufferLength;
	ULONG length = 0;

	switch (irpStackPointer->Parameters.DeviceIoControl.IoControlCode)
	{
	case DEVICE_SEND:
		KdPrint(("Sent data is %ws \r\n"));
		length = (wcnslen(buffer, 0x1FF) + 1) * 0x01 << 1;
		break;
	case DEVICE_RECEIVE:
		KdPrint(("Received data is %ws \r\n"));
		wcsncpy(buffer, sample, 0x1FF);
		length = (wcnslen(buffer, 0x1FF) + 1) * 0x01 << 1;
		break;
	default:
		status = STATUS_INVALID_PARAMETER;
		break;
	}

	ioRequestPacket->IoStatus.Information = length;
	ioRequestPacket->IoStatus.Status = status;

	IoCompleteRequest(ioRequestPacket, IO_NO_INCREMENT);

	return status;
}

NTSTATUS DispatchPassThru(PDEVICE_OBJECT deviceObject, PIRP ioRequestPacket)
{
	PIO_STACK_LOCATION irpStackPointer = IoGetCurrentIrpStackLocation(ioRequestPacket);
	NTSTATUS status = STATUS_SUCCESS;

	switch (irpStackPointer->MajorFunction)
	{
	case IRP_MJ_CREATE:
		KdPrint(("Creation Request \r\n"));
		break;

	case IRP_MJ_CLOSE:
		KdPrint(("Close Request \r\n"));
		break;

	case IRP_MJ_READ:
		KdPrint(("Read Request \r\n"));
		break;

	case IRP_MJ_WRITE:
		KdPrint(("Write Request \r\n"));
		break;

	default:
		status = STATUS_INVALID_PARAMETER;
		break;
	}

	ioRequestPacket->IoStatus.Information = STATUS_SUCCESS;
	ioRequestPacket->IoStatus.Status = status;

	IoCompleteRequest(ioRequestPacket, IO_NO_INCREMENT);

	return status;
}

VOID DriverUnload(PDRIVER_OBJECT driverObject)
{
	signal = TRUE;

	if (info)
		ExFreePool(info);

	for(int i = 0; i < cpuCount; i++) {
		KeWaitForSingleObject(threadObjects[i], Executive, KernelMode, FALSE, NULL);
		ObDereferenceObject(threadObjects[i]);
	}

	IoDeleteSymbolicLink(&symbolicLinkName);
	IoDeleteDevice(deviceObject);
	DbgPrint(("Driver Unloaded Successfully... \r\n"));
}

VOID CreateSystemThread()
{
	//TODO:
}